Vue.config.devtools = true;

function state_generate(states) {
  var start = `const state = {\n`;

  var middle = states.map(state => {
    var tpl = `  ${state}: '',\n`;
    return tpl;
  }).join('');

  var end = `}`;

  return start + middle + end;
}
function mutations_generate(states) {
  var start = `const mutations = {\n`;
  
  var middle = states.map(state => {
    var state_upper_case = String(state).toUpperCase();
    var tpl = `  UPDATE_${state_upper_case} (state, ${state}) {\n`  
            + `    state.${state} = ${state}\n`
            + `  },\n`;
    return tpl;
  }).join('');

  var end = `}`;

  return start + middle + end;
}
function actions_generate(states) {
  var start = `const actions = {\n`;

  var middle = states.map(state => {
    var state_upper_case = String(state).toUpperCase();
    var tpl = `  update_${state} ({commit}, ${state}) {\n`
      + `    commit('UPDATE_${state_upper_case}', ${state})\n`
      + `  },\n`;
    return tpl;
  }).join('');

  var end = `}`;

  return start + middle + end;
}
function export_generate(){
  var tpl = `export default {\n` 
          + `  state,\n`
          + `  mutations,\n`
          + `  actions\n`
          + `}`;
  return tpl;
}
function vuex_generate(states){
  return [ state_generate(states), 
           mutations_generate(states), 
           actions_generate(states),
           export_generate()].join('\n');
}
new Vue({
  el: '#app',
  data: {
    states: [{
      value:'name'
    },{
      value: ''
    }],
    code: ''
  },
  created(){
    this.generate_code();
  },
  methods: {
    add_state () {
      this.states.push({
        value: ''
      })
    },
    del_state (index) {
      this.states.splice(index, 1);
    },    
    generate_code() {
      var states = this.states
        .filter(item => {
          return item.value;
        })
        .map( item => {
          return item.value;
        })
      this.code =  vuex_generate(states)
    }
  }
})