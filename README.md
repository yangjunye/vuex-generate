# vuex-generate

根据 state 生成 mutation 和 actions

## 示例

输入 

```js
['id','name', 'age']
```

输出  

```js
const state = {
  id: '',
  name: '',
  age: '',
}
const mutations = {
  UPDATE_ID (state, id) {
    state.id = id
  },
  UPDATE_NAME (state, name) {
    state.name = name
  },
  UPDATE_AGE (state, age) {
    state.age = age
  },
}
const actions = {
  update_id ({commit}, id) {
    commit('UPDATE_ID', id)
  },
  update_name ({commit}, name) {
    commit('UPDATE_NAME', name)
  },
  update_age ({commit}, age) {
    commit('UPDATE_AGE', age)
  },
}
```